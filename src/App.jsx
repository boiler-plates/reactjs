import React from "react";
import Index from "client/index";
import "asset/css/index.less";

// Used for rule wrapping and hightest level logic.
const App = () => {
  return <Index />;
};

export default App;
