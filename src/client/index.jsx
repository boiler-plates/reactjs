import React from "react";

// entry level for rendering. usually for rounter
const Index = () => {
  return <div>Hello World</div>;
};

export default Index;
