const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { public } = require("../Helpers/path");

const plugins = [
  new MiniCssExtractPlugin({
    filename: "[name].css",
  }),
  new HtmlWebpackPlugin({
    template: public("index.html"),
    filename: "index.html",
    favicon: "./src/assets/images/favicon.ico",
  }),
];

module.exports = plugins;
