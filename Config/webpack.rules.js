const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const rules = [
  {
    test: /\.(js|jsx)$/,
    exclude: /node_modules/,
    use: "babel-loader",
  },
  {
    test: /\.less$/,
    use: [
      { loader: MiniCssExtractPlugin.loader },
      {
        loader: "css-loader",
        options: {
          sourceMap: false,
          url: false,
        },
      },
      {
        loader: "less-loader",
        options: {
          sourceMap: false,
          lessOptions: {
            javascriptEnabled: true,
          },
        },
      },
    ],
  },
  {
    test: /\.(png|j?g|svg|gif)?$/,
    use: "file-loader?name=./images/[name].[ext]",
  },
];

module.exports = rules;
