const { src, alias } = require("../Helpers/path");

const paths = {
  alias: {
    asset: src("assets"),
    language: src("locales"),
    ...alias,
  },
};

module.exports = paths;
