const { devServer } = require("../config.json");

const developmentConfig = {
  devServer: {
    historyApiFallback: true,
    port: 9000,
    open: true,
    hot: true,
    compress: true,
    ...devServer,
  },
};

module.exports = developmentConfig;
