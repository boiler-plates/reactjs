const TerserWebpackPlugin = require("terser-webpack-plugin");

const productionConfig = {
  optimization: {
    minimizer: [new TerserWebpackPlugin()],
  },
};

module.exports = productionConfig;
