const { root } = require("./Helpers/path");
const { main } = require("./package.json");
const { modes } = require("./config.json");
const rules = require("./Config/webpack.rules");
const plugins = require("./Config/webpack.plugins");
const { alias } = require("./Config/webpack.paths");
const productionConfig = require("./Config/webpack.production");
const developmentConfig = require("./Config/webpack.development");

const mode = process.env.NODE_ENV || modes.production;
const modeConfigs =
  mode === modes.development ? developmentConfig : productionConfig;

const config = {
  context: __dirname,
  mode: mode,
  entry: "./" + main,
  output: {
    path: root("dist"),
    filename: "[name].js",
    clean: true,
  },
  module: { rules: rules },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: alias,
  },
  plugins: plugins,
  ...modeConfigs,
};

module.exports = config;
